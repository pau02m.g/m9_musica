package Activitat2;

public class Conductor implements Runnable {

	int real_tempo;

	public Conductor(float tempo) {
		float m_tempo = 60000 / tempo;
		real_tempo = (int) m_tempo / 16;
	}

	@Override
	public void run() {

		try {
			for (;;) {
				Thread.sleep(real_tempo);
				synchronized (this) {
					// me espero el tempo que me toca
					this.notifyAll(); // notifico a las cosas
				}
			}
		} catch (InterruptedException e) {

			e.printStackTrace();
		}

	}

}
