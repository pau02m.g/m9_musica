package Activitat2;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class MainTest_1 {

	
	
	public static void main(String[] args) {

		ExecutorService executor = Executors.newCachedThreadPool();
		
		
		Note[] notesRythm =
			{
				new Note(Note.Frequency.F4, Note.Duration.corchea),
				new Note(Note.Frequency.F4, Note.Duration.corchea),
				new Note(Note.Frequency.F4, Note.Duration.corchea),
				new Note(Note.Frequency.Ds4, Note.Duration.corchea),
				new Note(Note.Frequency.E4, Note.Duration.blanca),
				new Note(Note.Frequency.E4, Note.Duration.corchea),
				new Note(Note.Frequency.E4, Note.Duration.corchea),
				new Note(Note.Frequency.E4, Note.Duration.corchea),
				new Note(Note.Frequency.D4, Note.Duration.corchea),
				new Note(Note.Frequency.Ds4, Note.Duration.corchea),
				new Note(Note.Frequency.Ds4, Note.Duration.corchea),
				new Note(Note.Frequency.E4, Note.Duration.corchea),
				new Note(Note.Frequency.F4, Note.Duration.corchea),
				new Note(Note.Frequency.Ds4, Note.Duration.corchea),
				new Note(Note.Frequency.Ds4, Note.Duration.corchea),
				new Note(Note.Frequency.Ds4, Note.Duration.corchea),
				new Note(Note.Frequency.Cs4, Note.Duration.corchea),
				new Note(Note.Frequency.D4, Note.Duration.corchea),
				new Note(Note.Frequency.E4, Note.Duration.corchea),
				new Note(Note.Frequency.Ds4, Note.Duration.corchea),
				new Note(Note.Frequency.D4, Note.Duration.corchea),
				new Note(Note.Frequency.F4, Note.Duration.corchea),
				new Note(Note.Frequency.Ds4, Note.Duration.corchea),
				new Note(Note.Frequency.D4, Note.Duration.corchea),
				new Note(Note.Frequency.Cs4, Note.Duration.corchea),
				
			};
		
		
		Conductor conductor = new Conductor(70);
		executor.execute(new Performer(notesRythm,conductor, 0));
		executor.execute(new Performer(notesRythm,conductor, 1));
			
		try {
			Thread.sleep(50);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	
		
		executor.execute(conductor);
		
		executor.shutdown();
		
		Thread conductorThread = new Thread(conductor);
		conductorThread.run();
		
		try {
			executor.awaitTermination(180, TimeUnit.SECONDS);
			executor.shutdownNow();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		

	}

}
