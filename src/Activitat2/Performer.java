package Activitat2;

import java.util.concurrent.Callable;

import com.sun.source.tree.SynchronizedTree;

public class Performer implements Runnable {

	// le paso una partitura al thread
	private Note[] partitura;

	private Conductor director;
	
	private int m_canal;

	public Performer(Note[] partitura_quelepaso, Conductor d, int canal) {
		partitura = partitura_quelepaso;
		director = d;
		m_canal = canal;
	}

	@Override
	public void run() {
		try {
			synchronized (director) {
				director.wait();
				
			}
		
			for (int i = 0; i < partitura.length; i++) {
				MidiPlayer.play(m_canal, partitura[i]);

				for (int j = partitura[i].getDuration(); j >= 0; j--) {

					synchronized (director) {
						director.wait();
						
					}
				}

				MidiPlayer.stop(m_canal, partitura[i]);

			}

		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

}
