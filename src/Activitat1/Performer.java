package Activitat1;

import java.util.concurrent.Callable;

public class Performer implements Runnable{

	
	// le paso una partitura al thread
	
	private Note[] partitura;
	
	private int tempo;
	
	public Performer(Note[] partitura_quelepaso, int t) {
		partitura = partitura_quelepaso;
		tempo = t;
	}

	@Override
	public void run() {
		
		float real = 60000/tempo;
		int real_tempo = (int)real/16;
		
		try
		{
			for(int i = 0; i < partitura.length; i++)
			{
				//MidiPlayer.setInstrument(instruments[0]);
			
				MidiPlayer.play(1, partitura[i]);
	
				
				Thread.sleep(real_tempo*partitura[i].getDuration());
				
				MidiPlayer.stop(1, partitura[i]);
			}
			
		}catch(InterruptedException e)
		{
			e.printStackTrace();
		}	
		
	}
	
	



}
